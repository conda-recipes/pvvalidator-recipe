# pvvalidator conda recipe

Home: "https://gitlab.esss.lu.se/icshwi/pvvalidator"

Package license: GPLv3

Recipe license: BSD 3-Clause

Summary: Tool to validate EPICS PVs based on ESS rules
